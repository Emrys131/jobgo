<?php
/**
 * Handler of messages
 * Catches data from ajax and call appropriate functions
 */

/* Requiring handler of database for calling functions */
require_once ('database_handler.php');

/* Checks if the message was sent */
if (isset($_POST['username']) && isset($_POST['text'])) {
    /* Gathering data for a message */
    $username = $_POST['username'];
    $text = $_POST['text'];
    /* Calling function to send a message */
    write_message($username, $text);
    /* Gathering filter data */
    $filter = $_POST['filter'];
    /* Calling function to update message */
    echo update_messages($filter);
/* Checks if messages update was called */
} else if (isset($_POST['update'])) {
    /* Gathering filter data */
    $filter = $_POST['filter'];
    /* Calling function to update messages */
    echo update_messages($filter);
/* Checks if message cleaner was called */
} else if (isset($_GET['clear'])) {
    /* Calling function to clean the messages */
    clear_messages();
    echo "Messages database cleared";
} else {
    echo "Error while message sending occurred";
}