/**
 * Sending message
 * Gathering data from html elements and sending to message handler
 */
function sendMessage() {
    var filter = filterMessages();
    var username = document.getElementById('message-username').value;
    var text = document.getElementById('message-text').value;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("messages-block").innerHTML = xmlhttp.responseText;
            document.getElementById("message-text").value = "";
            scrollBottom();
        }
    };
    xmlhttp.open("POST", "messages_handler.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("username=" + username + "&" + "text=" + text + "&" + "filter=" + filter);
}

/**
 * Handling enter key press to send a message
 *
 * @return boolean
 */
function pressEnter(e) {
    if(e.keyCode === 13){
        sendMessage();
    }
    return false;
}

/**
 * Updating messages every 0.5 of second
 */
function updateMessagesRecursive() {
    setTimeout(function () {
        updateMessagesOnce();
        updateMessagesRecursive();
        timeout();
    }, 500);
}

/**
 * Updating messages once
 * Calling message handler and returning data into message-block
 */
function updateMessagesOnce() {
    var filter = filterMessages();
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("messages-block").innerHTML = xmlhttp.responseText;
        }
    };
    xmlhttp.open("POST", "messages_handler.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("update&filter=" + filter);
}

/**
 * Checking current filter for messages
 *
 * @return string
 */
function filterMessages() {
    if (document.getElementById("filter-all").checked) {
        return "all";
    } else if (document.getElementById("filter-7days").checked) {
        return "7days";
    } else if (document.getElementById("filter-today").checked) {
        return "today";
    }
}

/**
 * Scrolling message block to the bottom
 */
function scrollBottom() {
    var block = document.getElementById('messages-block');
    block.scrollTop = block.scrollHeight;
}

/**
 * Calling function on page load
 */
function pageLoad() {
    updateMessagesOnce();
    updateMessagesRecursive();
    updateUsersList();
    updateUsersCounter();
    updateUsersListRecursive();
}

/**
 * Updating list of users online
 * Calling users handler and returning data into users list
 */
function updateUsersList() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("users-list").innerHTML = xmlhttp.responseText;
        }
    };
    xmlhttp.open("POST", "users_handler.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("update");
}

/**
 * Updating user counter
 * Calling user handler and returning data into user status
 */
function updateUsersCounter() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("users-status").innerHTML = xmlhttp.responseText;
        }
    };
    xmlhttp.open("POST", "users_handler.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("count");
}

/**
 * Updating online users every 5 seconds
 */
function updateUsersListRecursive() {
    setTimeout(function () {
        updateUsersList();
        updateUsersCounter();
        updateUsersListRecursive();
        timeout();
    }, 5000);
}

