<?php
/**
 * Message board page
 * Loads layout and scripts for message board
 */
session_start();
if (isset($_SESSION['username'])) {
    $username = $_SESSION['username'];
    /* Parameters for layout */
    $title = "Message board";
    /* Including style sheets */
    $style = array("style.css", "messageboard-style.css");
    /* Including js */
    $scripts = array("messageboard.js");
    /* Requiring top layout */
    require_once('views/layout_top.phtml');
    /* Requiring message board view */
    require_once('views/pages/messageboard_view.phtml');
    /* Requiring bottom layout */
    require_once('views/layout_bottom.phtml');
} else {
    header('Location: login.php');
}