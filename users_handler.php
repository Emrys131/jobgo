<?php
/**
 * Handler of messages
 * Catches data from ajax and call appropriate functions
 */

/* Requiring handler of database for calling functions */
require_once("database_handler.php");

session_start();

/* Checks if user needs to be set online */
if (isset($_SESSION['username']) && isset($_SESSION['online'])) {
    /* Gathering username */
    $username = $_SESSION['username'];
    /* Calling function to set user online */
    set_user_online($username);
    /* Unset online from session so current check would't be called */
    unset($_SESSION['online']);
}

/* Checks if users update was called */
if (isset($_POST['update'])) {
    /* Calling function to update users */
    echo update_users();
}

/* Checks if logout was called */
if (isset($_GET['logout'])) {
    /* Calling function to remove user from users database */
    remove_user($_SESSION['username']);
    session_unset();
    /* Redirecting to login page */
    header("Location: login.php");
}

/* Checks if users cleaner was called */
if (isset($_GET['clear'])) {
    /* Calling function to clean users from database*/
    clear_users();
    echo "Users database cleared";
}

/* Checks if users counter was called */
if (isset($_POST['count'])) {
    /* Calling function to count online users and pass data to users status */
    echo count_users();
}