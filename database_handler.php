<?php

/**
 * Acquire messages from database
 *
 * @return array
 */
function select_messages() {
    $data = file_get_contents("database/messages.php");
    return unserialize($data);
}

/**
 * Inserting new message into database
 *
 * @param string $username
 * @param string $text
 */
function write_message($username, $text) {
    $messages = select_messages();
    $message = array('name' => $username, 'text' => $text, 'time' => time());
    array_push($messages, $message);
    $input = serialize($messages);
    file_put_contents("database/messages.php", $input);
}

/**
 * Clearing messages from database
 */
function clear_messages() {
    $messages = array();
    $input = serialize($messages);
    file_put_contents("database/messages.php", $input);
}

/**
 * Updating messages
 * Function checks filter and according to it prints appropriate messages
 *
 * @param string $filter
 * @return string
 */
function update_messages($filter) {
    $messages = select_messages();
    $output = "";
    if ($filter == "7days") {
        foreach ($messages as $message) {
            if ($message['time'] > time() - 604800) {
                $output .= print_message($message);
            }
        }
    } else if ($filter == "today") {
        foreach ($messages as $message) {
            if ($message['time'] > strtotime("today midnight")) {
                $output .= print_message($message);
            }
        }
    } else {
        foreach ($messages as $message) {
            $output .= print_message($message);
        }
    }
    return $output;
}

/**
 * Printing one message into a string
 *
 * @param array $message
 * @return string
 */
function print_message($message) {
    $name = $message['name'];
    $text = $message['text'];
    $time = date('d.m.y, G:i', $message['time']);
    $output = "
        <div class=\"message\">
            <h1>$name - $time</h1>
            <p>
                $text
            </p>
        </div>
    ";
    return $output;
}

/**
 * Acquire users from database
 *
 * @return array
 */
function select_users() {
    $data = file_get_contents("database/users.php");
    return unserialize($data);
}

/**
 * Setting user online
 *
 * @param string $username
 */
function set_user_online($username) {
    $users = select_users();
    $user = array('username' => $username);
    array_push($users, $user);
    $input = serialize($users);
    file_put_contents("database/users.php", $input);
}

/**
 * Updating online user list
 *
 * @return string
 */
function update_users() {
    $users = select_users();
    $output = "";
    foreach ($users as $user) {
        $name = $user['username'];
        $output .= "
            <li>$name</li>
        ";
    }
    return $output;
}

/**
 * Removing user from online
 *
 * @param string $username
 */
function remove_user($username) {
    $users = select_users();
    foreach ($users as $key => $user) {
        if ($user['username'] == $username) {
            unset($users[$key]);
        }
    }
    $input = serialize($users);
    file_put_contents("database/users.php", $input);
}

/**
 * Clearing users from database
 */
function clear_users() {
    $users = array();
    $input = serialize($users);
    file_put_contents("database/users.php", $input);
}

/**
 * Counting how many users currently online
 *
 * @return string
 */
function count_users() {
    $users = select_users();
    $users_count = count($users);
    $output = "";
    if ($users_count > 1) {
        $output .= "<p>$users_count Users are logged in</p>";
    } else {
        $output .= "<p>Only $users_count user is logged in</p>";
    }
    return $output;
}